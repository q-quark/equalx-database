/**
 ** This file is part of the equalx project.
 ** Copyright 2020 Mihai Niculescu <q.quark@gmail.com>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include <iostream>

#include <array>
#include <memory>
#include <string_view>
#include <sstream>

#include <QDir>
#include <QDateTime>
#include <QFile>
#include <QFileInfo>
#include <QSqlDriver>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QVariant>

#include "BookmarksLibrary.h"

#define DB_OPEN(DB) \
    QSqlDatabase DB = QSqlDatabase::database(mConnectionName); \
    if(!DB.isOpen()) { \
    throw std::runtime_error("Cannot connect to BookmarksLibrary. The database is closed."); \
    }

#define DB_BEGIN_TRANSACTION(DB)  \
    bool changes_commited = false; \
    (DB).transaction(); \
    std::shared_ptr<void> cleanup_raii(nullptr, [&changes_commited, &DB](void*) { \
    if(!changes_commited) { \
    (DB).rollback(); \
    } \
    });


#define DB_END_TRANSACTION(DB)  \
    if(DB.commit()) { \
    changes_commited = true; \
    }


static constexpr std::array<std::string_view,6> create_bookmarks_library =
{{
     {"BEGIN TRANSACTION;"},
     {"CREATE TABLE bookmarks (id INTEGER primary key AUTOINCREMENT, idparent INTEGER, title VARCHAR(255), description TEXT, fileType CHAR(50), dirPath TEXT, created INTEGER, modified INTEGER,FOREIGN KEY(idparent) REFERENCES bookmarks_folders(id) ON DELETE CASCADE ON UPDATE CASCADE );"},
     {"CREATE TABLE bookmarks_content (id INTEGER primary key AUTOINCREMENT, refId INTEGER, latexContent TEXT, created INTEGER,FOREIGN KEY(refId) REFERENCES bookmarks(id) ON DELETE CASCADE ON UPDATE CASCADE );"},
     {"CREATE TABLE bookmarks_folders (id INTEGER primary key AUTOINCREMENT, idparent INTEGER, name VARCHAR(255), description VARCHAR(255), dirPath TEXT, created INTEGER, modified INTEGER,FOREIGN KEY(idparent) REFERENCES bookmarks_folders(id) ON DELETE CASCADE ON UPDATE CASCADE);"},
     {"INSERT INTO bookmarks_folders VALUES(1,NULL,'ROOT','',NULL,NULL,NULL);"},
     {"COMMIT;"}
 }};


namespace {

inline void to_bookmark(const QSqlQuery& query, Bookmark& bookmark, const std::filesystem::path& store_dir_path)
{
    bookmark.id        = query.value("id").toInt();
    bookmark.idparent  = query.value("idparent").toInt();
    bookmark.title     = query.value("title").toString();
    bookmark.description = query.value("description").toString();
    bookmark.content   = query.value("latexContent").toString();
    bookmark.filePath  = QString::fromStdString(store_dir_path / std::to_string(bookmark.id));
    bookmark.dirPath   = query.value("dirPath").toString();
    bookmark.created   = query.value("created").toLongLong();
    bookmark.modified  = query.value("modified").toLongLong();
}

} // end anon ns


const QString BookmarksLibrary::mConnectionName("bookmarks_connection");

BookmarksLibrary::BookmarksLibrary(const std::filesystem::path &database_path)
    : m_libraryFilePath{database_path}
{
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE", mConnectionName);

    // check Sql Driver required features
    if(!(db.driver()->hasFeature(QSqlDriver::Transactions))) {
        throw std::runtime_error("SQL Driver doesn't support Transactions");
    }

    if(!(db.driver()->hasFeature(QSqlDriver::PositionalPlaceholders))) {
        throw std::runtime_error("SQL Driver doesn't support PositionalPlaceholders");
    }

    if(!(db.driver()->hasFeature(QSqlDriver::LastInsertId))) {
        throw std::runtime_error("SQL Driver doesn't support LastInsertId");
    }

    // check existance and permissions of library database
    m_storeDirectoryPath = std::filesystem::absolute(m_libraryFilePath).parent_path();

    db.setDatabaseName( QString::fromStdString(m_libraryFilePath.string()) );

    if(!db.open()) {
        throw std::runtime_error("unable to open to library database");
    }

    DB_BEGIN_TRANSACTION(db);

    QSqlQuery query(db);
    if(!query.exec("PRAGMA foreign_keys=ON;")) {
        throw std::runtime_error("SQL Driver Failed Trying set FOREIGN KEYS=ON");
    }

    if(!query.exec("PRAGMA foreign_keys;")) {
        throw std::runtime_error("SQL Driver Failed Trying get FOREIGN KEYS status");
    }

    query.next();

    if(db.tables().isEmpty()) {
        createLibraryTables();
    }

    DB_END_TRANSACTION(db);
}

BookmarksLibrary::~BookmarksLibrary()
{
    QSqlDatabase::removeDatabase(mConnectionName);
}

uint BookmarksLibrary::countBookmarks() const
{
    DB_OPEN(db);

    DB_BEGIN_TRANSACTION(db);

    QSqlQuery query(db);
    query.prepare("SELECT COUNT(*) FROM bookmarks;");

    if(!query.exec()) {
        throw std::runtime_error("Cannot execute count bookmarks. Error: " + query.lastError().text().toStdString());
    }

    DB_END_TRANSACTION(db);

    if(!query.next()) {
        throw std::runtime_error("Could not count bookmarks. Error: " + query.lastError().text().toStdString());
    }

    return query.value(0).toUInt();
}

uint BookmarksLibrary::countFolders() const
{
    DB_OPEN(db);

    DB_BEGIN_TRANSACTION(db);

    QSqlQuery query(db);
    query.prepare("SELECT COUNT(*) FROM bookmarks_folders;");

    if(!query.exec()) {
        throw std::runtime_error("Cannot execute count folders. Error: " + query.lastError().text().toStdString());
    }

    DB_END_TRANSACTION(db);

    if(!query.next()) {
        throw std::runtime_error("Could not count folders. Error: " + query.lastError().text().toStdString());
    }

    return query.value(0).toUInt();
}

void BookmarksLibrary::createLibraryTables()
{
    QSqlDatabase db = QSqlDatabase::database(mConnectionName);
    if(!db.open())
        throw std::runtime_error("unable to connect to bookmarks library");

    QSqlQuery query(db);
    for(const std::string_view& line : create_bookmarks_library) {
        if(!query.exec(line.data())){
            throw std::runtime_error("Failed to exec the Sql Create database tables");
        }
    }
}

bool BookmarksLibrary::isValid(const Bookmark &bookmark)
{
    return bookmark.id > 0;
}

bool BookmarksLibrary::isValid(const BookmarkFolder &folder)
{
    return folder.id > 0;
}

void BookmarksLibrary::copyChildrenOf(int folderId, int toFolderId)
{
    if(folderId<=1 || toFolderId<1) return;

    DB_OPEN(db);

    // First copy all bookmarks of the current folder into folder toFolderId
    QSqlQuery query(db);
    query.prepare("SELECT id FROM bookmarks WHERE idparent=? ORDER BY created ASC");
    query.addBindValue(folderId);
    if(!query.exec()) {
        throw std::runtime_error("Failed to query bookmarks of current folder");
    }

    while(query.next()) {
        copyBookmark(query.value(0).toInt(), toFolderId);
    }

    // Lastly, copy all subfolder of the current folder into the folder toFolderId
    query.clear();
    query.prepare("SELECT id FROM bookmarks_folders WHERE idparent=? ORDER BY name ASC");
    query.addBindValue(folderId);

    if(!query.exec()) {
        throw std::runtime_error("Failed to query subfolders of current folder");
    }

    while(query.next()) {
        BookmarkFolder folder = getFolder(query.value(0).toInt());
        // copy the folder
        int newId = add(folder, toFolderId);
        copyChildrenOf(folder.id, newId); // copy all children of the folder into the copied folder of newID
    }
}

std::vector<std::filesystem::path> BookmarksLibrary::getAllBookmarksFileNames() const
{
    std::vector<std::filesystem::path> result;

    DB_OPEN(db);

    /** First, retrieve Bookmarks folders **/
    QSqlQuery query(db);
    query.prepare("SELECT id FROM bookmarks");
    if(!query.exec()) {
        throw std::runtime_error("Cannot query bookmarks. DB Error: " + query.lastError().text().toStdString());
    }

    while(query.next()) {
        const auto filePath = m_storeDirectoryPath / query.value(0).toString().toStdString();

        result.push_back(filePath);
    }

    return result;
}

int BookmarksLibrary::addBookmark(const QString &title, const QString &description, const QString &filePath, unsigned int parentId)
{
    Bookmark newBookmark;
    newBookmark.idparent    = parentId;
    newBookmark.title       = title;
    newBookmark.description = description;
    newBookmark.filePath    = filePath;

    return add(newBookmark, parentId);
}

int BookmarksLibrary::add(const Bookmark &bookmark, unsigned int parentId)
{
    int lastId=0;

    DB_OPEN(db);

    if(parentId==0) parentId=1;

    DB_BEGIN_TRANSACTION(db);

    QSqlQuery query(db);
    QString parentPath; // a null path means its parent is the root node

    if(parentId>1) { // parent is a folder
        query.prepare("SELECT dirPath FROM bookmarks_folders WHERE id=?");
        query.addBindValue(parentId);

        if(!query.exec()) {
            throw std::runtime_error("Cannot not find equation parent path.");
        }
        // parent found
        query.next();
        parentPath = QString("%1/%2").arg(query.value(0).toString()).arg(parentId);
    }

    if(!QFile::exists(bookmark.filePath)) {
        std::stringstream mess;
        mess << "Bookmarked file " << bookmark.filePath.toStdString() << " does not exits";

        throw std::runtime_error(mess.str());
    }

    QFileInfo fromFileInfo(bookmark.filePath);
    QString fileType    = fromFileInfo.completeSuffix();
    qint64 created      = QDateTime::currentMSecsSinceEpoch();
    qint64 modified     = created;

    query.clear();
    if(!query.prepare("INSERT INTO bookmarks (idparent, title, description, fileType, dirPath, created, modified) "
                      "values(?,?,?,?,?,?,?)")) {
        throw std::runtime_error("Cannot prepare equation insertion. " + query.lastError().text().toStdString());
    }
    query.addBindValue(parentId);
    query.addBindValue(bookmark.title);
    query.addBindValue(bookmark.description);
    query.addBindValue(fileType);
    query.addBindValue(parentPath);
    query.addBindValue(created);
    query.addBindValue(modified);

    if(!query.exec()) {
        throw std::runtime_error("Cannot insert equation into Bookmarks. ");
    }

    lastId = query.lastInsertId().toInt();

    // the query was successful, now copy the file to the bookmarks folder
    const auto filepath = m_storeDirectoryPath / std::to_string(lastId);

    if( !std::filesystem::copy_file(bookmark.filePath.toStdString(), filepath, std::filesystem::copy_options::overwrite_existing) ) {
        std::stringstream mess;
        mess << "Can not copy bookmark file " << bookmark.filePath.toStdString()
             << " to file: " << filepath.string();

        throw std::runtime_error(mess.str());
    }

    query.clear();
    query.prepare("INSERT INTO bookmarks_content (refId, latexContent, created) "
                  "values(?,?,?)");
    query.addBindValue(lastId);
    query.addBindValue(bookmark.content.simplified().replace(" ",""));
    query.addBindValue(created);

    if(!query.exec()) {
        throw std::runtime_error("Cannot add equation content to Bookmarks.");
    }

    DB_END_TRANSACTION(db);

    return lastId;
}

int BookmarksLibrary::addFolder(const QString &name, const QString &description, unsigned int parentId)
{
    BookmarkFolder folder;
    folder.idparent = parentId;
    folder.title     = name;
    folder.description = description;

    return add(folder, parentId);
}

int BookmarksLibrary::add(const BookmarkFolder &folder, unsigned int parentId)
{
    DB_OPEN(db);

    if(parentId==0) parentId=1;

    DB_BEGIN_TRANSACTION(db);

    QSqlQuery query(db);
    QString parentPath;

    if(parentId>1) { // parent is an existing folder (and not the ROOT)
        query.prepare("SELECT dirPath FROM bookmarks_folders WHERE id=?");
        query.addBindValue(parentId);

        if(!query.exec()){
            throw std::runtime_error("Cannot add folder to Bookmarks because I can not find its parent path. Database Reports: " + query.lastError().text().toStdString());
        }
        // parent found
        query.next();
        parentPath = QString("%1/%2").arg(query.value(0).toString()).arg(parentId);
    }

    query.clear();
    query.prepare("INSERT INTO bookmarks_folders (idparent,name,description,dirPath,created, modified) "
                  "VALUES (?,?,?,?,?,?)");
    query.addBindValue(parentId);
    query.addBindValue(folder.title);
    query.addBindValue(folder.description);
    query.addBindValue(parentPath);
    query.addBindValue(QDateTime::currentMSecsSinceEpoch());
    query.addBindValue(QDateTime::currentMSecsSinceEpoch());

    if(!query.exec()){
        throw std::runtime_error("Cannot add folder to Bookmarks. Database Reports: %1" + query.lastError().text().toStdString());
    }

    DB_END_TRANSACTION(db);

    return query.lastInsertId().toInt();
}

bool BookmarksLibrary::remove(const Bookmark &bookmark)
{
    return removeBookmark(bookmark.id);
}

bool BookmarksLibrary::remove(const BookmarkFolder &folder)
{
    return removeFolder(folder.id);
}

bool BookmarksLibrary::removeBookmark(unsigned int id)
{
    bool result = false;
    DB_OPEN(db);

    DB_BEGIN_TRANSACTION(db);

    QSqlQuery query(db);
    query.prepare("SELECT fileType,dirPath FROM bookmarks WHERE id=?");
    query.addBindValue(id);

    if(!query.exec()) {
        throw std::runtime_error("Cannot execute find bookmark. Error: " + query.lastError().text().toStdString());
    }

    if(!query.next()) { return result; }

    query.clear();
    query.prepare("DELETE FROM bookmarks WHERE id=?");
    query.addBindValue(id);

    if(!query.exec()) {
        throw std::runtime_error("Cannot delete bookmark. Error: " + query.lastError().text().toStdString());
    }

    result = true;

    // we dont throw: when Cannot delete bookmark from filesystem because the file might have been removed manually from FS..
    // and we want to clean correctly the DB
    result = result && std::filesystem::remove(m_storeDirectoryPath / std::to_string(id)); // delete bookmark file from FS

    DB_END_TRANSACTION(db);

    return result;
}

bool BookmarksLibrary::removeFolder(unsigned int id)
{
    // we dont allow removal of root folder..
    // but remove only its content (all bookmarks and all folders)
    if( id <= 1 ) {
        clear(CLEAR_METHOD::ClearAll);

        return false;
    }

    DB_OPEN(db);


    DB_BEGIN_TRANSACTION(db);

    QSqlQuery query(db);
    query.prepare("SELECT dirPath FROM bookmarks_folders WHERE id=?");
    query.addBindValue(id);

    if(!query.exec()) {
        throw std::runtime_error("Cannot select Folder from Bookmarks. Error: " + query.lastError().text().toStdString() );
    }

    if(!query.next()) return false;
    const QString dirPath = query.value(0).toString() + '/' + QString::number(id);

    // before deleteing the bookmarks, we must remove the bookmarks files from file system
    // find all bookmarks in this folder and **its subfolders**
    query.clear();
    query.prepare("SELECT id FROM bookmarks WHERE dirPath LIKE ? || '%' ");
    query.addBindValue(dirPath);
    if(!query.exec()) {
        throw std::runtime_error("Cannot find bookmarks in this Folder in Library. Error: " + query.lastError().text().toStdString() );
    }

    while(query.next()){
        const QString& fileId = query.value(0).toString();

        std::filesystem::remove(m_storeDirectoryPath / fileId.toStdString()); // delete bookmark file from FS
    }

    /****************************************/
    /* delete the folder from the database */
    /****************************************/
    query.clear();
    query.prepare("DELETE FROM bookmarks_folders WHERE id=?");
    query.addBindValue(id);

    if(!query.exec()){
        throw std::runtime_error("Cannot delete Folder from Bookmarks. Error: " + query.lastError().text().toStdString() );
    }

    DB_END_TRANSACTION(db);

    return true;
}

Bookmark BookmarksLibrary::getBookmark(unsigned int id) const
{
    Bookmark result;

    DB_OPEN(db);

    DB_BEGIN_TRANSACTION(db);

    QSqlQuery query(db);
    query.prepare("SELECT b.*,c.latexContent FROM bookmarks AS b INNER JOIN bookmarks_content AS c ON c.refId = b.id WHERE b.id=?");
    query.addBindValue(id);

    if(!query.exec()) {
        throw std::runtime_error("Cannot query Bookmarks. Error: " + query.lastError().text().toStdString());
    }

    DB_END_TRANSACTION(db);

    if(!query.next()) {
        return result;
    }


    to_bookmark(query, result, m_storeDirectoryPath);

    return result;
}

BookmarkFolder BookmarksLibrary::getFolder(unsigned int id) const
{
    DB_OPEN(db);

    DB_BEGIN_TRANSACTION(db);

    QSqlQuery query(db);
    query.prepare("SELECT idparent,name,description,dirPath,created,modified FROM bookmarks_folders where id=?");
    query.addBindValue(id);

    if(!query.exec()) {
        throw std::runtime_error("Cannot query Bookmarks. DB Error: " + query.lastError().text().toStdString());
    }

    DB_END_TRANSACTION(db);

    if(!query.next()) {
        return BookmarkFolder();
    }

    BookmarkFolder f;
    f.id = id;
    f.idparent  = query.value(0).toInt();
    f.title      = query.value(1).toString();
    f.description   = query.value(2).toString();
    f.dirPath       = query.value(3).toString();
    f.created       = query.value(4).toLongLong();
    f.modified      = query.value(5).toLongLong();

    return f;
}

void BookmarksLibrary::update(const Bookmark &bookmark)
{
    DB_OPEN(db);
    DB_BEGIN_TRANSACTION(db);

    // we DO NOT want to re-parent the bookmark, we simply want to update its own fields
    QSqlQuery query(db);
    query.prepare("UPDATE bookmarks SET title=?,description=?,modified=? WHERE id=?");
    query.addBindValue(bookmark.title);
    query.addBindValue(bookmark.description);
    query.addBindValue(QDateTime::currentMSecsSinceEpoch());
    query.addBindValue(bookmark.id);

    if(!query.exec()) {
        throw std::runtime_error("Cannot update Bookmarks. Error: " + query.lastError().text().toStdString());
    }

    DB_END_TRANSACTION(db);
}

void BookmarksLibrary::update(const BookmarkFolder &folder)
{
    DB_OPEN(db);
    DB_BEGIN_TRANSACTION(db);

    // we DO NOT want to re-parent the bookmark, we simply want to update its own fields
    QSqlQuery query(db);
    query.prepare("UPDATE bookmarks_folders SET name=?,description=?,modified=? WHERE id=?");
    query.addBindValue(folder.title);
    query.addBindValue(folder.description);
    query.addBindValue(QDateTime::currentMSecsSinceEpoch());
    query.addBindValue(folder.id);

    if( !query.exec() ) {
        throw std::runtime_error("Cannot update Folders from Bookmarks. Error: " + query.lastError().text().toStdString());
    }

    DB_END_TRANSACTION(db);
}

void BookmarksLibrary::clear(BookmarksLibrary::CLEAR_METHOD method)
{
    // fetch filenames for all bookmarks stored
    auto fileNamesList = getAllBookmarksFileNames();

    // clean the database
    DB_OPEN(db);

    DB_BEGIN_TRANSACTION(db);

    QSqlQuery sql_query(db);

    switch (method) {
    case CLEAR_METHOD::ClearAll:
        sql_query.prepare("DELETE FROM bookmarks");
        if(!sql_query.exec()) {
            throw std::runtime_error("Could not execute delete all from bookmarks");
        }

        sql_query.clear();
        sql_query.prepare("DELETE FROM bookmarks_content");
        if(!sql_query.exec()) {
            throw std::runtime_error("Could not execute delete all from bookmarks_content");
        }

        sql_query.clear();
        sql_query.prepare("DELETE FROM bookmarks_folders WHERE id>1");
        if(!sql_query.exec()) {
            throw std::runtime_error("Could not execute delete all from bookmarks_folders");
        }

        break;
    default:
        break;
    }

    DB_END_TRANSACTION(db);

    // remove files from the filesystem
    for(const auto& path : fileNamesList) {
        std::filesystem::remove(path);
    }
}

unsigned int BookmarksLibrary::countChildren(unsigned int folderId) const
{
    unsigned int nChildren = 0;

    DB_OPEN(db);

    /** Count Bookmarks folders **/
    QSqlQuery query(db);
    query.prepare("SELECT COUNT(id) FROM bookmarks_folders WHERE idparent=?");
    query.addBindValue(folderId);

    if(!query.exec()) {
        throw std::runtime_error("Cannot query size of subfolders in bookmarks_folders. DB Error: " + query.lastError().text().toStdString());
    }

    if(query.next())
        nChildren = query.value(0).toUInt();

    /** Count Bookmarks **/
    query.clear();
    query.prepare("SELECT COUNT(id) FROM bookmarks WHERE idparent=?");
    query.addBindValue(folderId);

    if(!query.exec()) {
        throw std::runtime_error("Cannot query size of bookmarks in bookmarks_folder. DB Error: " + query.lastError().text().toStdString());
    }

    if(query.next()) nChildren += query.value(0).toUInt();

    return nChildren;
}

std::vector<Bookmark> BookmarksLibrary::getBookmarksInFolder(unsigned int folderId) const
{
    std::vector<Bookmark> result;

    DB_OPEN(db);

    /** First, retrieve Bookmarks folders **/
    QSqlQuery query(db);
    query.prepare("SELECT id,title,description,dirPath,created,modified FROM bookmarks WHERE idparent=? ORDER BY created DESC");
    query.addBindValue(folderId);
    if(!query.exec()) {
        throw std::runtime_error("Cannot query bookmarks. DB Error: " + query.lastError().text().toStdString());
    }

    int count_rows = query.size();
    if(count_rows > 0) result.resize(count_rows);

    while(query.next()) {
        Bookmark b;
        b.id = query.value(0).toInt();
        b.idparent = folderId;
        b.title = query.value(1).toString();
        b.description=query.value(2).toString();

        const auto filePath = m_storeDirectoryPath / std::to_string(b.id);

        b.filePath  = QString::fromStdString(filePath.string());
        b.dirPath   = query.value(3).toString();
        b.created   = query.value(4).toLongLong();
        b.modified  = query.value(5).toLongLong();

        result.push_back(b);
    }

    return result;
}

std::vector<BookmarkFolder> BookmarksLibrary::getSubFoldersInFolder(unsigned int folderId) const
{
    std::vector<BookmarkFolder> result;

    DB_OPEN(db);

    /** First, retrieve Bookmarks folders **/
    QSqlQuery query(db);
    query.prepare("SELECT id,name,description,dirPath,created,modified FROM bookmarks_folders WHERE idparent=? ORDER BY created DESC");
    query.addBindValue(folderId);
    if(!query.exec()) {
        throw std::runtime_error("Cannot query bookmarks_folders. DB Error: " + query.lastError().text().toStdString());
    }

    int count_rows = query.size();
    if(count_rows > 0) result.resize(count_rows);

    while(query.next()) {
        BookmarkFolder f;
        f.id = query.value(0).toInt();
        f.idparent  = folderId;
        f.title      = query.value(1).toString();
        f.description   = query.value(2).toString();
        f.dirPath       = query.value(3).toString();
        f.created       = query.value(4).toLongLong();
        f.modified      = query.value(5).toLongLong();

        result.push_back(f);
    }

    return result;
}

int BookmarksLibrary::copyBookmark(int id, int toNewParentId)
{
    const Bookmark& bookmark = getBookmark(id);

    if(!isValid(bookmark)){ // we have valid objects in database
        throw std::runtime_error("Invalid bookmark id");
    }

    int newId = add(bookmark, toNewParentId);

    // the query was successful, now copy the file to the bookmarks folder
    const auto toFile = m_storeDirectoryPath / std::to_string(newId); // each file copyied is named after its index in the table

    std::filesystem::copy_file(bookmark.filePath.toStdString(), toFile, std::filesystem::copy_options::overwrite_existing);

    return newId;
}

int BookmarksLibrary::copyFolder(int id, int toNewParentId)
{
    const BookmarkFolder folder = getFolder(id);
    const BookmarkFolder toParentfolder = getFolder(toNewParentId);

    if(!isValid(folder) || !isValid(toParentfolder)){ // we have valid objects in database
        throw std::runtime_error("Invalid bookmark folders id");
    }
    int newid = add(folder, toNewParentId);

    copyChildrenOf(id, newid);

    return newid;
}

void BookmarksLibrary::moveBookmark(unsigned int id, unsigned int newParentId)
{
    /****************************************************************************************/
    /* moving means: We want to re-parent the bookmark                                      */
    /*--------------------------------------------------------------------------------------*/
    /* - we must also change the its field 'dirPath' in database                            */
    /*--------------------------------------------------------------------------------------*/

    DB_OPEN(db);

    DB_BEGIN_TRANSACTION(db);

    // First lets find the current dirPath
    QSqlQuery query(db);
    query.prepare("SELECT dirPath FROM bookmarks WHERE id=?");
    query.addBindValue(id);

    if(!query.exec()) {
        throw std::runtime_error("Cannot find bookmark. Error: " + query.lastError().text().toStdString());
    }

    if(!query.next()){
        // cannot find required bookmark. Does it even exits? think not.
        return;
    }
    // required bookmark exists. current dirPath is:
    const QString oldDirPath = query.value(0).toString();

    QString newDirPath;
    if(newParentId>1) { // move bookmark to a subfolder of the root
        // Now, find its new dirPath
        query.clear();
        query.prepare("SELECT dirPath FROM bookmarks_folders WHERE id=?");
        query.addBindValue(newParentId);

        if(!query.exec()){
            throw std::runtime_error("Cannot find Folder in Bookmarks. Error: "  + query.lastError().text().toStdString());
        }

        if(!query.next()){
            // Cannot fetch dirPath of new parent Folder in Bookmarks.
            return;
        }

        newDirPath = query.value(0).toString() + '/' + QString::number(newParentId);
    }
    // now update bookmark to the new parent ID and new dirPath
    query.clear();
    query.prepare("UPDATE bookmarks SET idparent=?,dirPath=?,modified=? WHERE id=?");
    query.addBindValue(newParentId);
    query.addBindValue(newDirPath);
    query.addBindValue(QDateTime::currentMSecsSinceEpoch());
    query.addBindValue(id);

    if(!query.exec()){
        throw std::runtime_error("Cannot move/reparent bookmark . Error: " + query.lastError().text().toStdString());
    }

    DB_END_TRANSACTION(db);
}

void BookmarksLibrary::moveFolder(int id, int newParentId)
{
    /****************************************************************************************/
    /* We want to re-parent the folder                                                      */
    /*--------------------------------------------------------------------------------------*/
    /* - we must also change the field 'dirPath' in database of its subfolders and bookmarks*/
    /*--------------------------------------------------------------------------------------*/

    if(id<=1) { // invalid id's or root folder does not move
        return;
    }


    DB_OPEN(db);

    DB_BEGIN_TRANSACTION(db);

    // First lets find the current folders 'dirPath'
    QSqlQuery query(db);
    query.prepare("SELECT dirPath FROM bookmarks_folders WHERE id=?");
    query.addBindValue(id);

    if(!query.exec()) {
        throw std::runtime_error("Cannot find Folder in Bookmarks. Error: " + query.lastError().text().toStdString());
    }

    if(!query.next()) {
        // folder with id does not exists
        return;
    }
    const QString oldDirPath = query.value(0).toString() + '/' + QString::number(id);

    QString newDirPath; // the dirPath of the new parent folder
    if(newParentId>1) {
        // then lets find the new parents 'dirPath'
        query.clear();
        query.prepare("SELECT dirPath FROM bookmarks_folders WHERE id=?");
        query.addBindValue(newParentId);

        if(!query.exec()){
            throw std::runtime_error("Cannot find parent of Folder from Bookmarks. Error: " + query.lastError().text().toStdString());
        }

        if(!query.next()) {
            // folder with newParentId does not exists
            return;
        }
        newDirPath = query.value(0).toString() + '/' + QString::number(newParentId);
    }

    // now update folder to the new parent ID and new dirPath
    query.clear();
    query.prepare("UPDATE bookmarks_folders SET idparent=?,dirPath=? WHERE id=?");
    query.addBindValue(newParentId);
    query.addBindValue(newDirPath);
    query.addBindValue(id);

    if(!query.exec()){
        throw std::runtime_error("Cannot reparent Folder from Bookmarks. Error: " + query.lastError().text().toStdString());
    }

    // now update all subfolders to the new dirPath
    newDirPath = newDirPath + '/' +  QString::number(id);
    query.clear();
    query.prepare("UPDATE bookmarks_folders SET dirPath=replace(dirPath,?,?)  WHERE dirPath LIKE ? || '%' ");
    query.addBindValue(oldDirPath);
    query.addBindValue(newDirPath);
    query.addBindValue(oldDirPath);

    if(!query.exec()){
        throw std::runtime_error("Cannot reparent subfolders of this Folder from Bookmarks. Error: " + query.lastError().text().toStdString());
    }

    // now update all bookmarks from this folder and its subfolders to the new dirPath
    query.clear();
    query.prepare("UPDATE bookmarks SET dirPath=replace(dirPath,?,?) WHERE dirPath LIKE ? || '%' ");
    query.addBindValue(oldDirPath);
    query.addBindValue(newDirPath);
    query.addBindValue(oldDirPath);

    if(!query.exec()){
        throw std::runtime_error("Cannot reparent subfolders of this Folder from Bookmarks. Error: " + query.lastError().text().toStdString());
    }

    DB_END_TRANSACTION(db);
}

std::vector<Bookmark> BookmarksLibrary::searchBookmarks(const QString& searchText, BookmarksLibrary::SEARCH_MODE searchType) const
{
    DB_OPEN(db);

    std::vector<Bookmark> result;

    QSqlQuery q(db);
    QString searchQuery;

    switch (searchType) {
    case SEARCH_BY_TITLE:
        searchQuery = "SELECT b.*,c.latexContent FROM bookmarks AS b INNER JOIN bookmarks_content AS c ON c.refId = b.id  WHERE b.title LIKE '%' || ? || '%' ORDER BY b.created DESC";
        break;
    case SEARCH_BY_DESCRIPTION:
        searchQuery = "SELECT b.*,c.latexContent FROM bookmarks AS b INNER JOIN bookmarks_content AS c ON c.refId = b.id  WHERE b.description LIKE '%' || ? || '%' ORDER BY b.created DESC";
        break;
    case SEARCH_BY_CONTENT:
        searchQuery = "SELECT b.*,c.latexContent FROM bookmarks AS b INNER JOIN bookmarks_content AS c ON c.refId = b.id  WHERE c.latexContent LIKE '%' || ? || '%' ORDER BY b.created DESC";
        break;
    default: // Title and Description
        searchQuery = "SELECT b.*,c.latexContent FROM bookmarks AS b INNER JOIN bookmarks_content AS c ON c.refId = b.id  WHERE (b.title LIKE '%' || ? || '%') | (b.description LIKE '%' || ? || '%') ORDER BY b.created DESC";
        break;
    }

    q.prepare(searchQuery);
    q.addBindValue(searchText);
    if(searchType == SEARCH_BY_TITLE_DESCRIPTION) {
        q.addBindValue(searchText);
    }

    if( !q.exec() ) {
        throw std::runtime_error("Cannot access Bookmarks. Error: " + q.lastError().text().toStdString() );
    }

    const int count_rows = q.size();
    if(count_rows > 0) result.resize(count_rows);

    while(q.next()) {
        Bookmark allocated;
        to_bookmark(q, allocated, m_storeDirectoryPath);

        result.push_back(allocated);
    }

    return result;
}

bool BookmarksLibrary::is_readable(const std::filesystem::path &filepath)
{
    return ((std::filesystem::status(filepath).permissions() & std::filesystem::perms::owner_read) != std::filesystem::perms::none);
}

bool BookmarksLibrary::is_writable(const std::filesystem::path &filepath)
{
    return ((std::filesystem::status(filepath).permissions() & std::filesystem::perms::owner_write) != std::filesystem::perms::none);
}
