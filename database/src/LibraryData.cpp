#include <filesystem>

#include <QDate>

#include "LibraryData.h"

bool is_valid(const Bookmark &bookmark) noexcept
{
    const QDate& date_now = QDate::currentDate();

    const bool has_valid_id{bookmark.id>0};
    const bool has_valid_parent_id{bookmark.idparent > 0};
    const bool has_valid_title{!bookmark.title.isEmpty()};
    const bool has_existent_file{std::filesystem::exists(bookmark.filePath.toStdString())};
    const bool was_created_in_the_past{QDate::fromJulianDay(bookmark.created) < date_now};
    const bool was_modified_in_the_past{QDate::fromJulianDay(bookmark.modified) < date_now};

    return has_valid_id &&
            has_valid_parent_id &&
            has_valid_title &&
            has_existent_file &&
            was_created_in_the_past &&
            was_modified_in_the_past;
}

bool is_valid(const BookmarkFolder &folder) noexcept
{
    if(folder.id == 1 && folder.title == "ROOT") return true;

    const QDate& date_now = QDate::currentDate();

    const bool has_valid_id{folder.id>0};
    const bool has_valid_parent_id{folder.idparent > 0};
    const bool has_valid_name{!folder.title.isEmpty()};
    const bool was_created_in_the_past{QDate::fromJulianDay(folder.created) < date_now};
    const bool was_modified_in_the_past{QDate::fromJulianDay(folder.modified) < date_now};

    return has_valid_id &&
            has_valid_parent_id &&
            has_valid_name &&
            was_created_in_the_past &&
            was_modified_in_the_past;
}

bool is_similar(const Bookmark &b1, const Bookmark &b2)
{
    return (b1.title == b2.title) &&
            (b1.description == b2.description) &&
            (b1.content == b2.content);
}

bool is_similar(const BookmarkFolder &b1, const BookmarkFolder &b2)
{
    return (b1.title == b2.title) &&
            (b1.description == b2.description);
}
