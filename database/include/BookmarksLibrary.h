/**
 ** This file is part of the equalx project.
 ** Copyright 2020 Mihai Niculescu <q.quark\gmail.com>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#ifndef BOOKMARKSLIBRARY_H
#define BOOKMARKSLIBRARY_H

#include <vector>
#include <filesystem>

#include <QString>

#include "LibraryData.h"

/*! \class BookmarksLibrary
 *  \brief Implements a Bookmarks Library structured in categories (called folders) in a tree like structure
 *  \details the files that are bookmarked are registered and copied in a storage directory
 * on the filesystem (see getStorageDirectoryPath()).
 * Only Folders can contain children, wether other Folders and/or Bookmarks
 */
class BookmarksLibrary {
public:
    enum CLEAR_METHOD {
        ClearAll // we remove all bookmarks and folders, except the root folder
    };

    enum SEARCH_MODE {
        SEARCH_BY_TITLE_DESCRIPTION,
        SEARCH_BY_TITLE,
        SEARCH_BY_DESCRIPTION,
        SEARCH_BY_CONTENT
    };

    explicit BookmarksLibrary(const std::filesystem::path& database_path);
    ~BookmarksLibrary();

    std::filesystem::path getDatabaseFilePath() const { return m_libraryFilePath; }
    std::filesystem::path getStoreDirectoryPath() const { return m_storeDirectoryPath; }

    uint countBookmarks() const;
    uint countFolders() const;

    /*! \brief Add a bookmark in the database
     *  \details The bookmark can have a folder/category as parent is specified by 'folderId'. If successful,
     * it copies the file 'file' to the folder returned by \a getStoreDirectoryPath(). If a file already exists
     * at that location it is overwritten
     *
     * If 'folderId'= 1, then its parent is the root node.
     *
     * \param filePath - a valid path on the filesystem to be added to library
     *
     * \return If everything went well, it returns the id of the equation in DB.
     * If not it returns -1;
     *
     * \throws std::runtime_error
     * \sa getStoreDirectoryPath()
     */
    int addBookmark(const QString& title, const QString& description, const QString& filePath, unsigned int folderId=1);
    int add(const Bookmark& bookmark, unsigned int folderId=1);

    /*! \brief Add a folder/category in library. If successful, create
     * a subfolder to the folder which has id. If 'folderId'= 1, then its
     * parent is the root node.
     *
     * \return If everything went well it returns the id of the folder in DB
     * If not it returns -1;
     *
     * \throws std::runtime_error
     */
    int addFolder(const QString& name, const QString& description = "", unsigned int folderId=1);
    int add(const BookmarkFolder& folder, unsigned int folderId=1);

    bool remove(const Bookmark& bookmark);
    bool remove(const BookmarkFolder& folder);

    /*!
     * \brief removeBookmark
     * \param id
     * \return true if bookmark was removed successfully, false otherwise
     * \throws std::runtime_error
     */
    bool removeBookmark(unsigned int id);
    bool removeFolder(unsigned int id);

    /*!
     * \brief Get a bookmark with id \a id from the library
     * \param id
     * \return a bookmark - if the bookmark is a valid record than it passes is_valid(bookmark),
     * if there is no bookmark with \a id then it returns an invalid bookmark
     *
     * \throws std::runtime_error if database could not interogated
     *
     * \sa is_valid(Bookmark)
     */
    Bookmark getBookmark(unsigned int id) const;
    BookmarkFolder getFolder(unsigned int id) const;

    //! \brief update bookmark
    //! \note the only affected fields are \a title , \a description and \a modified, the others are not updated
    //! \throws std::runtime_error if it could not update the record
    void update(const Bookmark &bookmark);

    //! \brief we simply want to update its own fields
    //! \note we DO NOT want to re-parent the bookmark no matter of idparent
    void update(const BookmarkFolder &folder);

    //------------------------------------------------
    // Complex Operations on Database
    //------------------------------------------------

    /*!
     * \brief clear - removes all bookmarks and folders from the library.
     * \details It also removes all bookmark files stored at getStoreDirectoryPath()
     * \param method
     * \throws std::runtime_error if an error happened during cleaning the database
     * \throws std::filesystem_error if an error happened when removing files from the filesystem
     */
    void clear(BookmarksLibrary::CLEAR_METHOD method = ClearAll);

    //! \brief returns the direct number of children in folder (subfolders + bookmarks)
    unsigned int countChildren(unsigned int folderId=1) const;

    //! \brief returns a list with all direct bookmarks in folder id
    std::vector<Bookmark> getBookmarksInFolder(unsigned int folderId=1) const;

    //! \brief returns a list with all direct sub-folders in folder id
    std::vector<BookmarkFolder> getSubFoldersInFolder(unsigned int folderId=1) const;

    int copyBookmark(int id, int toNewfolderId);
    int copyFolder(int id, int toNewfolderId);

    void moveBookmark(unsigned int id, unsigned int newfolderId);
    void moveFolder(int id, int newfolderId);

    std::vector<Bookmark> searchBookmarks(const QString &searchText, SEARCH_MODE searchType=SEARCH_BY_TITLE_DESCRIPTION) const;
private:
    static bool is_readable(const std::filesystem::path & filepath);
    static bool is_writable(const std::filesystem::path & filepath);

    void createLibraryTables();
    bool isValid(const Bookmark& bookmark);
    bool isValid(const BookmarkFolder& folder);
    void copyChildrenOf(int folderId, int toFolderId);
    std::vector<std::filesystem::path> getAllBookmarksFileNames() const;

    static const QString mConnectionName; //! \brief name of the database connection name

    const std::filesystem::path m_libraryFilePath;
    std::filesystem::path m_storeDirectoryPath; // the directory in which to save files (bookmarks, etc)
};

#endif // BOOKMARKSLIBRARY_H

