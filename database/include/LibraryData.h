#ifndef LIBRARYDATA_H
#define LIBRARYDATA_H

#include <QString>

struct Bookmark
{
    int id;
    int idparent;
    QString title;
    QString description;
    QString content; // latex content of the equation (not whole latex doc)
    QString filePath; // path to a valid file on the disk
    QString dirPath;
    qint64 created;
    qint64 modified;
};

struct BookmarkFolder
{
    int id;
    int idparent;
    QString title;
    QString description;
    QString dirPath;
    qint64 created;
    qint64 modified;
};

/*!
 * \brief A simple check if the bookmark can be a valid record in the Library
 * \details A bookmark is considered valid if:
 * \li has an id greater than 0
 * \li has a valid idparent (> 0)
 * \li has non empty title
 * \li a file exists at path Bookmark::filePath
 * \li created data is in the past
 *
 * \param bookmark - the bookmark to check
 *
 * \return true if the bookmark is valid, false otherwise
 */
bool is_valid(const Bookmark& bookmark) noexcept;

/*!
 * \brief A simple check if the bookmark folder can be a valid record in the Library
 * \details A bookmark Folder is considered valid if:
 * \li has an id greater than 0
 * \li has a valid idparent (> 0)
 * * \li has non empty name
 * \li created data is in the past
 *
 * \param bookmark - the bookmark to check
 *
 * \return true if the \a folder is valid, false otherwise
 */
bool is_valid(const BookmarkFolder& folder) noexcept;

/*!
 * \brief Check if the two bookmarks are similar
 * \details Bookmarks are considered similar if all are true:
 * \li have the same title
 * \li have the same description
 * \li have the same content
 *
 * \param b1 - the bookmark
 * \param b2 - the bookmark
 *
 * \return true if the bookmarks are similar, false otherwise
 */
bool is_similar(const Bookmark& b1, const Bookmark& b2);

/*!
 * \brief Check if the two folders are similar
 * \details Bookmarks Folders are considered similar if all are true:
 * \li have the same title
 * \li have the same description
 *
 * \param b1 - the bookmark folder
 * \param b2 - the bookmark folder
 *
 * \return true if the bookmarks are similar, false otherwise
 */
bool is_similar(const BookmarkFolder& b1, const BookmarkFolder& b2);
#endif // LIBRARYDATA_H
