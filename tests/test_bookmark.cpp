#include <vector>
#include <string>

#include <QString>

#include <boost/filesystem.hpp>

#include <gtest/gtest.h>

#include "BookmarksLibrary.h"

// might need this
std::ostream& operator<<(std::ostream& os, const QString& str)
{
    if(os) {
        os << str.toStdString();
    }

    return os;
}

class EqDatabaseFixture : public ::testing::Test
{
public:

    EqDatabaseFixture()
        : library(lib_path.string())
    { }

    ~EqDatabaseFixture()
    {
        library.clear();

        boost::filesystem::remove(lib_path);

        for(const auto& e : filesToRemove) {
            boost::filesystem::remove(e);
        }
    }

    std::string create_temp_file() {
        const auto tfilename = boost::filesystem::temp_directory_path() / boost::filesystem::unique_path();
        filesToRemove.push_back(tfilename);

        const char data = {'c'};

        boost::filesystem::ofstream mytempfile;
        mytempfile.open(tfilename, std::ios_base::ate);
        mytempfile.write(&data, 1);
        mytempfile.close();

        return tfilename.string();
    }


protected:
    const boost::filesystem::path lib_path{"bookmarks.db"};
    BookmarksLibrary library;

private:
    std::vector<boost::filesystem::path> filesToRemove;
};

class EqDatabaseFixtureIntParam : public EqDatabaseFixture, public testing::WithParamInterface<int>
{

};

TEST(equalx_database, constructor)
{
    ASSERT_NO_THROW(BookmarksLibrary b("bookmarks.db"));
}

TEST_F(EqDatabaseFixture, db_exists_on_filesystem )
{
    ASSERT_EQ( library.getDatabaseFilePath(), "bookmarks.db" );
    ASSERT_TRUE( std::filesystem::exists(library.getDatabaseFilePath()) );
}

TEST_F(EqDatabaseFixture, store_directory_is_valid)
{
    ASSERT_EQ( library.getDatabaseFilePath(), "bookmarks.db" );
    ASSERT_TRUE( std::filesystem::exists(library.getStoreDirectoryPath()) );
    ASSERT_TRUE( std::filesystem::is_directory(library.getStoreDirectoryPath()));
}

TEST_F(EqDatabaseFixture, database_is_empty)
{
    ASSERT_NO_THROW(library.countBookmarks());
    ASSERT_NO_THROW(library.countFolders());
    ASSERT_EQ( library.countBookmarks(), 0);
    ASSERT_EQ( library.countFolders(), 1); // we have the default root folder
}

TEST_F(EqDatabaseFixture, add_bookmark)
{
    const auto tfile = create_temp_file();

    ASSERT_TRUE( boost::filesystem::exists(tfile) );

    uint result_id;

    ASSERT_NO_THROW( result_id = library.addBookmark("My Bookmark", "A good equation", QString::fromStdString(tfile)) );
    ASSERT_GT( result_id, 0);
    ASSERT_EQ( library.countBookmarks(), 1);
}

TEST_F(EqDatabaseFixture, add_more_bookmarks)
{
    const int N = 10;
    for(int i=0; i< N; ++i){
        const auto tfile = create_temp_file();

        ASSERT_TRUE( boost::filesystem::exists(tfile) );

        uint result_id;

        ASSERT_NO_THROW( result_id = library.addBookmark("My Bookmark", "A good equation", QString::fromStdString(tfile)) );
        ASSERT_GT( result_id, 0);
        ASSERT_EQ( library.countBookmarks(), i+1);
    }
}

TEST_F(EqDatabaseFixture, clear_all)
{
    // first add some records to the database and storage
    const int N = 10;
    for(int i=0; i< N; ++i){
        const auto tfile = create_temp_file();

        ASSERT_TRUE( boost::filesystem::exists(tfile) );

        uint result_id;

        ASSERT_NO_THROW( result_id = library.addBookmark("My Bookmark", "A good equation", QString::fromStdString(tfile)) );
        ASSERT_GT( result_id, 0);
        ASSERT_EQ( library.countBookmarks(), i+1);
    }

    ASSERT_NO_THROW( library.clear() );
    ASSERT_EQ(library.countBookmarks(), 0);
    ASSERT_EQ(library.countFolders(), 1); // root node should remain
}

TEST_F(EqDatabaseFixture, remove_bookmark)
{
    const auto tfile = create_temp_file();

    ASSERT_TRUE( boost::filesystem::exists(tfile) );

    uint result_id;

    ASSERT_NO_THROW( result_id = library.addBookmark("My Bookmark", "A good equation", QString::fromStdString(tfile)) );
    ASSERT_GT( result_id, 0);
    ASSERT_EQ( library.countBookmarks(), 1);

    bool status = false;

    ASSERT_NO_THROW( status = library.removeBookmark(result_id) );
    EXPECT_TRUE(status);
    ASSERT_EQ( library.countBookmarks(), 0);
}

TEST_F(EqDatabaseFixture, get_bookmark)
{
    const auto tfile = create_temp_file();

    ASSERT_TRUE( boost::filesystem::exists(tfile) );

    uint result_id;

    ASSERT_NO_THROW( result_id = library.addBookmark("My Bookmark", "A good equation", QString::fromStdString(tfile)) );
    ASSERT_GT( result_id, 0);
    ASSERT_EQ( library.countBookmarks(), 1);

    Bookmark bookmark;

    ASSERT_NO_THROW( bookmark = library.getBookmark(result_id) );
    EXPECT_TRUE(is_valid(bookmark));
}

TEST_F(EqDatabaseFixture, update_bookmark)
{
    // first create a bookmark file
    const auto tfile = create_temp_file();

    ASSERT_TRUE( boost::filesystem::exists(tfile) );

    uint result_id;

    // add the file as bookmark
    ASSERT_NO_THROW( result_id = library.addBookmark("My Bookmark", "A good equation", QString::fromStdString(tfile)) );
    ASSERT_GT( result_id, 0);
    ASSERT_EQ( library.countBookmarks(), 1);

    // get the bookmark record
    Bookmark before;

    ASSERT_NO_THROW( before = library.getBookmark(result_id) );
    EXPECT_TRUE(is_valid(before));

    // update the fields
    Bookmark updater{before}; // we do this to have the same bookmark.id
    updater.title = "New title";
    updater.description = "more better description";

    // bad changes (that should not be used in the update process)
    updater.idparent = 65496;
    updater.content = "more better description";
    updater.filePath = "more better description";
    updater.dirPath = "more better description";
    updater.created = -1;
    updater.modified = -1;

    // do the update
    ASSERT_NO_THROW( library.update(updater) );

    // Now lets check the update

    // Get the updated bookmark record
    Bookmark after;

    ASSERT_NO_THROW( after = library.getBookmark(result_id) );
    EXPECT_TRUE(is_valid(after));

    // these fields should change after update
    EXPECT_EQ(updater.id, after.id);
    EXPECT_EQ(updater.title, after.title);
    EXPECT_EQ(updater.description, after.description);

    // these fields should NOT be changed by the update..
    EXPECT_NE(updater.idparent, after.idparent);
    EXPECT_NE(updater.content, after.content);
    EXPECT_NE(updater.filePath, after.filePath);
    EXPECT_NE(updater.dirPath, after.dirPath);
    EXPECT_NE(updater.created, after.created);
    EXPECT_NE(updater.modified, after.modified);

    // so, should be the same as before the update
    EXPECT_EQ(before.idparent, after.idparent);
    EXPECT_EQ(before.content, after.content);
    EXPECT_EQ(before.filePath, after.filePath);
    EXPECT_EQ(before.dirPath, after.dirPath);
    EXPECT_EQ(before.created, after.created);
}

TEST_P(EqDatabaseFixtureIntParam, remove_nonexistent_bookmark)
{
    bool status = false;

    ASSERT_NO_THROW( status = library.removeBookmark(GetParam()) );
    EXPECT_FALSE(status);
}

TEST_P(EqDatabaseFixtureIntParam, get_nonexistent_bookmark)
{
    Bookmark bookmark;

    ASSERT_NO_THROW( bookmark = library.getBookmark( GetParam()) );
    EXPECT_FALSE(is_valid(bookmark));
}

//-----------------------------------------------------------------------------
// ----  Tests for the folders
//-----------------------------------------------------------------------------

TEST_F(EqDatabaseFixture, add_folder)
{
    uint result_id;

    ASSERT_NO_THROW( result_id = library.addFolder("My Category", "A folder is a category") );
    ASSERT_GT( result_id, 1); // id 1 is reserved for the root node/folder
    ASSERT_EQ( library.countFolders(), 2);
}

TEST_F(EqDatabaseFixture, add_more_folders_to_root)
{
    const int N = 10;
    for(int i=1; i< N; ++i) {
        uint result_id;

        ASSERT_NO_THROW( result_id = library.addFolder("My Category", "A folder is a category") );
        ASSERT_GT( result_id, 1);
        ASSERT_EQ( library.countFolders(), i+1);
    }
}

TEST_F(EqDatabaseFixture, add_more_folders_nested)
{
    const int N = 10;

    for(int i=1; i<= N; ++i) {
        uint result_id = 1;

        ASSERT_NO_THROW( result_id = library.addFolder("My Category", "A folder is a category", result_id) );
        ASSERT_GT( result_id, 1);
        ASSERT_EQ( library.countFolders(), i+1);
    }
}

TEST_F(EqDatabaseFixture, remove_folder)
{
    uint result_id;

    ASSERT_NO_THROW( result_id = library.addFolder( "My Category", "A folder is a category" ) );
    ASSERT_GT( result_id, 1);
    ASSERT_EQ( library.countFolders(), 2);

    bool status = false;

    ASSERT_NO_THROW( status = library.removeFolder(result_id) );
    EXPECT_TRUE(status);
    ASSERT_EQ( library.countFolders(), 1);
}

TEST_F(EqDatabaseFixture, root_folder_exists)
{
    BookmarkFolder folder;

    ASSERT_NO_THROW( folder = library.getFolder(1) );
    EXPECT_TRUE(is_valid(folder));

    EXPECT_EQ(folder.id, 1);
    EXPECT_EQ(folder.idparent, 0);
    EXPECT_EQ(folder.title, "ROOT");
}

TEST_F(EqDatabaseFixture, get_folder)
{
    uint result_id;

    ASSERT_NO_THROW( result_id = library.addFolder("My Category", "A folder is a category") );
    ASSERT_GT( result_id, 1);
    ASSERT_EQ( library.countFolders(), 2);

    BookmarkFolder folder;

    ASSERT_NO_THROW( folder = library.getFolder(result_id) );
    EXPECT_TRUE(is_valid(folder));
}

TEST_F(EqDatabaseFixture, update_folder)
{
    // first create a bookmark file
    uint result_id;

    // add the file as bookmark
    ASSERT_NO_THROW( result_id = library.addFolder("My Category", "A folder is a category") );
    ASSERT_GT( result_id, 1);
    ASSERT_EQ( library.countFolders(), 2);

    // get the bookmark record
    BookmarkFolder before;

    ASSERT_NO_THROW( before = library.getFolder(result_id) );
    EXPECT_TRUE( is_valid(before) );

    // update the fields
    BookmarkFolder updater{before}; // we do this to have the same bookmark.id
    updater.title = "New title";
    updater.description = "more better description";

    // bad changes (that should not be used in the update process)
    updater.idparent = 65496;
    updater.description = "more better description";
    updater.dirPath = "more better path";
    updater.created = -1;
    updater.modified = -1;

    // do the update
    ASSERT_NO_THROW( library.update(updater) );

    // Now lets check the update

    // Get the updated bookmark record
    BookmarkFolder after;

    ASSERT_NO_THROW( after = library.getFolder(result_id) );
    EXPECT_TRUE( is_valid(after) );

    // these fields should change after update
    EXPECT_EQ(updater.id, after.id);
    EXPECT_EQ(updater.title, after.title);
    EXPECT_EQ(updater.description, after.description);

    // these fields should NOT be changed by the update..
    EXPECT_NE(updater.idparent, after.idparent);
    EXPECT_NE(updater.dirPath, after.dirPath);
    EXPECT_NE(updater.created, after.created);
    EXPECT_NE(updater.modified, after.modified);

    // so, should be the same as before the update
    EXPECT_EQ(before.idparent, after.idparent);
    EXPECT_EQ(before.dirPath, after.dirPath);
    EXPECT_EQ(before.created, after.created);
}

TEST_P(EqDatabaseFixtureIntParam, remove_nonexistent_folder)
{
    bool status = false;

    ASSERT_NO_THROW( status = library.removeFolder(GetParam()) );
    EXPECT_FALSE(status);
}

TEST_P(EqDatabaseFixtureIntParam, get_nonexistent_folder)
{
    BookmarkFolder folder;

    ASSERT_NO_THROW( folder = library.getFolder( GetParam()) );
    EXPECT_FALSE( is_valid(folder) );
}

INSTANTIATE_TEST_SUITE_P(TestNonExistentBookmark, EqDatabaseFixtureIntParam, testing::Values(0, -1, 2, 7, 10));

//-----------------------------------------------------------------------------
// ----  Tests for the Complex operations
//-----------------------------------------------------------------------------

class EqDBFilledComplexTreeFixture : public EqDatabaseFixture
{
public:
    EqDBFilledComplexTreeFixture()
        : EqDatabaseFixture(),
          _number_of_bookmarks{0},
          _number_of_folders{1},
          _id_general_folder{0},
          _id_physics_folder{0}
    {
        /*
         *----------- Create a Tree of Bookmarks and subfolders
         *
         *                                      root/
         *                                        |
         *            --------------------------------------------------------------------------------
         *            |               |                 |                 |                          |
         *         physics/     My Bookmark 1     My Bookmark 2     My Bookmark 3                 general/
         *            |                                                                               |
         *     ---------------                                       ---------------------------------------------------------------
         *     |             |                                       |                |                  |             |           |
         * Newton's Eq    Sch. Eq                                  Thesis       Best PhD Thesis         Doc.        Manuals/     Notes/
         *
         */

        // create 3 bookmarks file in the root folder
        create_bookmark("My Bookmark 1", "A good equation");
        create_bookmark("My Bookmark 2", "A too good equation");
        create_bookmark("My Bookmark 3", "A very good equation");

        // Create 2 folders as children of the root Folder
        _id_general_folder = create_folder("General", "First category", 1);
        _id_physics_folder = create_folder("Physics", "Second folder", 1);

        // create 3 bookmarks in the 'General' Folder
        create_bookmark("Thesis", "Bachelor Thesis", _id_general_folder);
        create_bookmark("Best PhD thesis", "my PhD. thesis ", _id_general_folder);
        create_bookmark("Documentation", "A list of documentation for", _id_general_folder);

        // create 2 subfolder for 'General' Folder
        _id_manuals_folder = create_folder("Manuals", "manuals for school", _id_general_folder);
        _id_notes_folder   = create_folder("Notes", "Small general notes", _id_general_folder);

        // create 2 bookmarks in the 'Physics' Folder
        create_bookmark("Newton's Equation", "for the second law of mechanics", _id_physics_folder);
        create_bookmark("Schrödinger equation", "the basis of quantum mechanics", _id_physics_folder);
    }

    ~EqDBFilledComplexTreeFixture()
    {

    }

    uint create_bookmark(const QString& name, const QString& description, uint parentId=1);
    uint create_folder(const QString& name, const QString& description, uint parentId=1);

    uint general_folder_id() const { return _id_general_folder; }
    uint physics_folder_id() const { return _id_physics_folder; }
    uint manuals_folder_id() const { return _id_manuals_folder; }
    uint notes_folder_id() const { return _id_notes_folder; }
private:
    uint _number_of_bookmarks;
    uint _number_of_folders;

    uint _id_general_folder;
    uint _id_physics_folder;
    uint _id_manuals_folder;
    uint _id_notes_folder;
};


uint EqDBFilledComplexTreeFixture::create_bookmark(const QString &name, const QString &description, uint parentId)
{
    uint result_id = 0;
    const auto temp_file = EqDatabaseFixture::create_temp_file();

    EXPECT_TRUE( boost::filesystem::exists(temp_file) );
    EXPECT_NO_THROW( result_id = EqDatabaseFixture::library.addBookmark(name, description,
                                                                        QString::fromStdString(temp_file),
                                                                        parentId) );
    EXPECT_GT( result_id, 0);

    ++_number_of_bookmarks;

    EXPECT_EQ( library.countBookmarks(), _number_of_bookmarks);

    return result_id;
}

uint EqDBFilledComplexTreeFixture::create_folder(const QString &name, const QString &description, uint parentId)
{
    uint result_id = 0;

    EXPECT_NO_THROW( result_id = EqDatabaseFixture::library.addFolder(name, description, parentId) );
    EXPECT_GT( result_id, 1);

    ++_number_of_folders;

    EXPECT_EQ( library.countFolders(), _number_of_folders);

    return result_id;
}

TEST_F(EqDBFilledComplexTreeFixture, count)
{
    ASSERT_EQ( library.countBookmarks() ,  8);
    ASSERT_EQ( library.countFolders() ,  5);

    ASSERT_EQ( library.countChildren(1) ,  5);
    ASSERT_EQ( library.countChildren(general_folder_id()) ,  5);
    ASSERT_EQ( library.countChildren(physics_folder_id()) ,  2);

    ASSERT_EQ( library.countChildren(manuals_folder_id()) , 0);
    ASSERT_EQ( library.countChildren(notes_folder_id()) ,  0);

    ASSERT_EQ( library.countChildren(-234234) ,  0);
}

TEST_F(EqDBFilledComplexTreeFixture, check_fixture)
{
    ASSERT_EQ( library.countChildren(1) ,  5);
    ASSERT_EQ( library.countBookmarks() ,  8);
    ASSERT_EQ( library.countFolders() ,  5);
    ASSERT_GT( general_folder_id(), 1 );
    ASSERT_GT( physics_folder_id(), 1 );
    ASSERT_EQ( library.countChildren(general_folder_id()) , 5);
    ASSERT_EQ( library.countChildren(physics_folder_id()) , 2);

    // Check children of Root Folder
    const auto& bookmarks = library.getBookmarksInFolder();
    EXPECT_EQ(bookmarks[0].title, "My Bookmark 3");
    EXPECT_EQ(bookmarks[0].idparent, 1);
    EXPECT_EQ(bookmarks[0].description, "A very good equation");

    EXPECT_EQ(bookmarks[1].title, "My Bookmark 2");
    EXPECT_EQ(bookmarks[1].idparent, 1);
    EXPECT_EQ(bookmarks[1].description, "A too good equation");

    EXPECT_EQ(bookmarks[2].title, "My Bookmark 1");
    EXPECT_EQ(bookmarks[2].idparent, 1);
    EXPECT_EQ(bookmarks[2].description, "A good equation");

    const auto& folders = library.getSubFoldersInFolder();
    EXPECT_EQ(folders[0].title, "Physics");
    EXPECT_EQ(folders[0].idparent, 1);
    EXPECT_EQ(folders[0].description, "Second folder");

    EXPECT_EQ(folders[1].title, "General");
    EXPECT_EQ(folders[1].idparent, 1);
    EXPECT_EQ(folders[1].description, "First category");

    // Check children of General folder
    const uint general_id = general_folder_id();
    const auto& general_bookmarks = library.getBookmarksInFolder(general_id);
    EXPECT_EQ(general_bookmarks[0].title, "Documentation");
    EXPECT_EQ(general_bookmarks[0].idparent, general_id);
    EXPECT_EQ(general_bookmarks[0].description, "A list of documentation for");

    EXPECT_EQ(general_bookmarks[1].title, "Best PhD thesis");
    EXPECT_EQ(general_bookmarks[1].idparent, general_id);
    EXPECT_EQ(general_bookmarks[1].description, "my PhD. thesis ");

    EXPECT_EQ(general_bookmarks[2].title, "Thesis");
    EXPECT_EQ(general_bookmarks[2].idparent, general_id);
    EXPECT_EQ(general_bookmarks[2].description, "Bachelor Thesis");

    const auto& general_folders = library.getSubFoldersInFolder(general_id);
    EXPECT_EQ(general_folders[0].title, "Notes");
    EXPECT_EQ(general_folders[0].idparent, general_id);
    EXPECT_EQ(general_folders[0].description, "Small general notes");

    EXPECT_EQ(general_folders[1].title, "Manuals");
    EXPECT_EQ(general_folders[1].idparent, general_id);
    EXPECT_EQ(general_folders[1].description, "manuals for school");

    // Check children of Physics folder
    const uint physics_id = physics_folder_id();
    const auto& physics_bookmarks = library.getBookmarksInFolder(physics_id);
    EXPECT_EQ(physics_bookmarks[0].title, "Schrödinger equation");
    EXPECT_EQ(physics_bookmarks[0].idparent, physics_id);
    EXPECT_EQ(physics_bookmarks[0].description, "the basis of quantum mechanics");

    EXPECT_EQ(physics_bookmarks[1].title, "Newton's Equation");
    EXPECT_EQ(physics_bookmarks[1].idparent, physics_id);
    EXPECT_EQ(physics_bookmarks[1].description, "for the second law of mechanics");
}

TEST_F(EqDBFilledComplexTreeFixture, clear_database)
{
    ASSERT_EQ( library.countChildren(1) ,  5);
    ASSERT_EQ( library.countBookmarks() ,  8);
    ASSERT_EQ( library.countFolders() ,  5);

    library.clear();

    ASSERT_EQ( library.countChildren(1) ,  0);
    ASSERT_EQ( library.countBookmarks() ,  0);
    ASSERT_EQ( library.countFolders() ,  1);
}

TEST_F(EqDBFilledComplexTreeFixture, search_bookmarks)
{
    ASSERT_EQ( library.countChildren(1) ,  5);
    ASSERT_EQ( library.countBookmarks() ,  8);
    ASSERT_EQ( library.countFolders() ,  5);

    const auto& found_bookmarks = library.searchBookmarks("bookmark", BookmarksLibrary::SEARCH_MODE::SEARCH_BY_TITLE);
    ASSERT_EQ(found_bookmarks.size(), 3);

    const auto& found_bookmarked_thesis = library.searchBookmarks("thesis", BookmarksLibrary::SEARCH_MODE::SEARCH_BY_TITLE);
    ASSERT_EQ(found_bookmarked_thesis.size(), 2);

    const auto& found_bookmarked_equation = library.searchBookmarks("equation", BookmarksLibrary::SEARCH_MODE::SEARCH_BY_TITLE);
    ASSERT_EQ(found_bookmarked_equation.size(), 2);

    const auto& found_bookmarked_doc = library.searchBookmarks("documentation", BookmarksLibrary::SEARCH_MODE::SEARCH_BY_TITLE);
    ASSERT_EQ(found_bookmarked_doc.size(), 1);

    const auto& found_bookmarked_mechanics = library.searchBookmarks("mechanics", BookmarksLibrary::SEARCH_MODE::SEARCH_BY_DESCRIPTION);
    ASSERT_EQ(found_bookmarked_mechanics.size(), 2);

    const auto& found_bookmarked_desc_eq = library.searchBookmarks("equation", BookmarksLibrary::SEARCH_MODE::SEARCH_BY_DESCRIPTION);
    ASSERT_EQ(found_bookmarked_desc_eq.size(), 3);

    const auto& found_title_desc_eq = library.searchBookmarks("equation", BookmarksLibrary::SEARCH_MODE::SEARCH_BY_TITLE_DESCRIPTION);
    ASSERT_EQ(found_title_desc_eq.size(), 5);
}

TEST_F(EqDBFilledComplexTreeFixture, copy_bookmarks)
{
    ASSERT_EQ( library.countChildren(1) ,  5);
    ASSERT_EQ( library.countBookmarks() ,  8);
    ASSERT_EQ( library.countFolders() ,  5);

    const auto& found_bookmarks = library.searchBookmarks("My Bookmark 1", BookmarksLibrary::SEARCH_MODE::SEARCH_BY_TITLE);
    ASSERT_EQ(found_bookmarks.size(), 1);

    const Bookmark& orig_bookmark = found_bookmarks.at(0);

    ASSERT_TRUE(is_valid(orig_bookmark));

    uint copied_bookmark_id;

    ASSERT_NO_THROW( copied_bookmark_id = library.copyBookmark(orig_bookmark.id, general_folder_id()) );
    ASSERT_GT(copied_bookmark_id, 0);

    const Bookmark& copied_bookmark = library.getBookmark(copied_bookmark_id);

    ASSERT_EQ(copied_bookmark.id, copied_bookmark_id);
    ASSERT_EQ(copied_bookmark.idparent, general_folder_id());
    ASSERT_TRUE(is_similar(orig_bookmark, copied_bookmark));

    ASSERT_EQ( library.countBookmarks() ,  9);
}

TEST_F(EqDBFilledComplexTreeFixture, copy_folder)
{
    ASSERT_EQ( library.countChildren(1) ,  5);
    ASSERT_EQ( library.countBookmarks() ,  8);
    ASSERT_EQ( library.countFolders() ,  5);

    const auto& folder = library.getFolder( general_folder_id() );

    ASSERT_TRUE( is_valid(folder) );

    uint copied_folder_id;
    ASSERT_NO_THROW( copied_folder_id = library.copyFolder(folder.id, physics_folder_id()) );
    ASSERT_GT(copied_folder_id, 0);

    const BookmarkFolder& copied_folder = library.getFolder(copied_folder_id);

    ASSERT_EQ(copied_folder.id, copied_folder_id);
    ASSERT_EQ(copied_folder.idparent, physics_folder_id());
    ASSERT_TRUE(is_similar(copied_folder, copied_folder));

    ASSERT_EQ( library.countFolders() ,  8);
    ASSERT_EQ( library.countBookmarks() , 11);
}

TEST_F(EqDBFilledComplexTreeFixture, move_bookmarks)
{
    ASSERT_EQ( library.countChildren(1) ,  5);
    ASSERT_EQ( library.countBookmarks() ,  8);
    ASSERT_EQ( library.countFolders() ,  5);

    const auto& found_bookmarks = library.searchBookmarks("My Bookmark 1", BookmarksLibrary::SEARCH_MODE::SEARCH_BY_TITLE);
    ASSERT_EQ(found_bookmarks.size(), 1);

    const Bookmark& orig_bookmark = found_bookmarks.at(0);

    ASSERT_TRUE(is_valid(orig_bookmark));

    ASSERT_NO_THROW( library.moveBookmark(orig_bookmark.id, general_folder_id()) );

    const Bookmark&  moved_bookmark = library.getBookmark(orig_bookmark.id);

    ASSERT_TRUE( is_valid( moved_bookmark) );
    ASSERT_EQ(moved_bookmark.id, orig_bookmark.id);
    ASSERT_EQ(moved_bookmark.idparent, general_folder_id());
    ASSERT_TRUE(is_similar(orig_bookmark, moved_bookmark));

    ASSERT_EQ( library.countBookmarks() ,  8);
    ASSERT_EQ( library.countChildren(1) ,  4);
    ASSERT_EQ( library.countChildren(general_folder_id()) ,  6);
}

TEST_F(EqDBFilledComplexTreeFixture, move_folder)
{
    ASSERT_EQ( library.countChildren(1) ,  5);
    ASSERT_EQ( library.countBookmarks() ,  8);
    ASSERT_EQ( library.countFolders() ,  5);

    const auto& folder = library.getFolder( general_folder_id() );

    ASSERT_TRUE( is_valid(folder) );

    ASSERT_NO_THROW( library.moveFolder(folder.id, physics_folder_id()) );

    const BookmarkFolder& moved_folder = library.getFolder(folder.id);

    ASSERT_EQ(moved_folder.id, folder.id);
    ASSERT_EQ(moved_folder.idparent, physics_folder_id());
    ASSERT_TRUE(is_similar(moved_folder, folder));

    ASSERT_EQ( library.countFolders() ,  5);
    ASSERT_EQ( library.countBookmarks() , 8);
    ASSERT_EQ( library.countChildren(1) ,  4);
    ASSERT_EQ( library.countChildren(physics_folder_id()) ,  3);
}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

